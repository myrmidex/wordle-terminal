<?php

namespace WordleTerminal\Guess\Exceptions;

use Exception;

class InvalidGuessException extends Exception
{
    protected $message = 'INVALID_GUESS';
}
