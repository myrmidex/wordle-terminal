<?php

namespace WordleTerminal\Guess;

class GuessValidator
{
    public function validate(string $guess): bool
    {
        $chars = preg_replace('/[^a-z]/i', '', $guess);

        if (strlen($chars) !== strlen($guess)) return false;

        if (strlen($guess) !== 5) return false;

        return true;
    }
}