<?php

namespace WordleTerminal\Word\Exceptions;

use Exception;

class WordlistProviderUnreachableException extends Exception
{
    protected $message = 'WORDLIST_PROVIDER_UNREACHABLE';
}
