<?php

namespace WordleTerminal\Word;

use WordleTerminal\Word\Exceptions\WordlistProviderUnreachableException;

class Fetch
{
    protected const WORDLIST_SOURCE = 'https://gist.githubusercontent.com/dracos/dd0668f281e685bad51479e5acaadb93/raw/6bfa15d263d6d5b63840a8e5b64e04b382fdb079/valid-wordle-words.txt';

    /**
     * @throws WordlistProviderUnreachableException
     */
    public function word(): string
    {
        return array_rand($this->fetchWordList());
    }

    /**
     * @throws WordlistProviderUnreachableException
     */
    private function fetchWordList(): array
    {
        $wordlistContent = file_get_contents(self::WORDLIST_SOURCE);

        if ($wordlistContent === false) {
            throw new WordlistProviderUnreachableException();
        }

        return explode("\n", $wordlistContent);
    }
}