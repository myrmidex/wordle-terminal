<?php

namespace WordleTerminal;

use WordleTerminal\Guess\Exceptions\InvalidGuessException;
use WordleTerminal\Guess\GuessResponse;
use WordleTerminal\Guess\GuessValidator;
use WordleTerminal\Word\Exceptions\WordlistProviderUnreachableException;
use WordleTerminal\Word\Fetch;

class Wordle
{
    public ?string $word = null;
    public array $guesses = [];

    /**
     * @throws WordlistProviderUnreachableException
     */
    public function __construct()
    {
        $this->word = (new Fetch())->word();
    }

    /**
     * @throws InvalidGuessException
     */
    public function guess(string $guess): GuessResponse
    {
        // guess validation
        if ((new GuessValidator)->validate($guess) === false) {
            throw new InvalidGuessException();
        }

        $this->guesses[] = $guess;

        return new GuessResponse();
    }
}