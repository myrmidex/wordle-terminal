<?php

namespace WordleTerminal\Tests\Integration;

use WordleTerminal\Tests\TestCase;
use WordleTerminal\Wordle;

class NewGameTest extends TestCase
{
    public function test_new_game_generates_word(): void
    {
        $newGame = new Wordle();

        $this->assertNotNull($newGame->word);
    }

    public function test_first_guess_shows_up_in_guesses(): void
    {
        $word = substr(implode('', $this->faker->words(5)), 0, 5);

        $game = new Wordle();
        $game->guess($word);

        $this->assertEquals(
            [$word],
            $game->guesses
        );

    }
}
