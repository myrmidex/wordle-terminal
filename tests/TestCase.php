<?php

namespace WordleTerminal\Tests;

use PHPUnit\Framework\TestCase as TestCaseOriginal;

class TestCase extends TestCaseOriginal
{
    protected \Faker\Generator $faker;

    public function __construct(...$args)
    {
        parent::__construct(...$args);

        $this->faker = \Faker\Factory::create();
    }
}
