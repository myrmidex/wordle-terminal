<?php

namespace Unit\Guess\Exceptions;

use PHPUnit\Framework\TestCase;
use WordleTerminal\Guess\Exceptions\InvalidGuessException;

class InvalidGuessExceptionTest extends TestCase
{
    public function test_exception_message(): void
    {
        $this->expectException(InvalidGuessException::class);
        $this->expectExceptionMessage('INVALID_GUESS');

        throw new InvalidGuessException();
    }
}
