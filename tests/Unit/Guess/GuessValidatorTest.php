<?php

namespace Unit\Guess;

use PHPUnit\Framework\TestCase;
use WordleTerminal\Guess\GuessValidator;

class GuessValidatorTest extends TestCase
{
    /**
     * @dataProvider invalidGuessesData
     */
    public function test_exception_message(string $guess, bool $expected): void
    {
        $this->assertEquals(
            $expected,
            (new GuessValidator())->validate($guess)
        );
    }


    public static function invalidGuessesData(): array
    {
        return [
            ['guess' => '', 'expected' => false],
            ['guess' => 'a', 'expected' => false],
            ['guess' => 'ab', 'expected' => false],
            ['guess' => 'abc', 'expected' => false],
            ['guess' => 'abcd', 'expected' => false],
            ['guess' => 'abcd#', 'expected' => false],
            ['guess' => 'abcde', 'expected' => true],
            ['guess' => 'ab#de', 'expected' => false],
            ['guess' => 'abcdef', 'expected' => false],
            ['guess' => 'abcdefg', 'expected' => false],
            ['guess' => 'abcdefgh', 'expected' => false],
        ];
    }
}
