<?php

namespace WordleTerminal\Tests\unit\Exceptions;

use PHPUnit\Framework\TestCase;
use WordleTerminal\Word\Exceptions\WordlistProviderUnreachableException;

class WordlistProviderUnreachableExceptionTest extends TestCase
{
    public function test_exception_message(): void
    {
        $this->expectException(WordlistProviderUnreachableException::class);
        $this->expectExceptionMessage('WORDLIST_PROVIDER_UNREACHABLE');

        throw new WordlistProviderUnreachableException();
    }
}
