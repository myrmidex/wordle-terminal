<?php

namespace WordleTerminal\Tests\Unit;

use WordleTerminal\Guess\Exceptions\InvalidGuessException;
use WordleTerminal\Tests\TestCase;
use WordleTerminal\Wordle;

class WordleTest extends TestCase
{
    /**
     * @test
     * @dataProvider invalidGuessesData
     */
    public function test_guess_throws_exception_if_invalid_length_or_type(string $guess): void
    {
        $this->expectException(InvalidGuessException::class);

        $game = new Wordle();
        $game->guess($guess);
    }

    public static function invalidGuessesData(): array
    {
        return [
            [''],
            ['a'],
            ['ab'],
            ['abc'],
            ['abcd'],
            ['abcdef'],
            ['abcdefg'],
            ['abcdefgh'],
        ];
    }
}